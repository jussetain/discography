# Setup the project

- Pull from the repo
- Run `npm install`
- Run `npm run start` for the front (runs on port 3000)

For the back, you can either use a local instance or the deployed one on my server.
By default, the deployed one is set, if you want to use a local one, change server address from the IP address written to `localhost` in the file `src/constants/URI.js`
Both are on port 3333.

- You can run tests with `npm run test`