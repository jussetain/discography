import { combineReducers } from 'redux'
import artist from './artist'

/*
    Combines all reducers (only one in this case)
 */
const reduxApp = combineReducers({
    artist,
})

export default reduxApp