import React from 'react'
import "./Artist.css"
import { connect } from 'react-redux'
import URI from "../../constants/URI"

/*
    Component for the discography
 */
class Artist extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            albums: []
        }
    }

    componentDidMount(){
        let self = this ;
        let { artist } = this.props ;

        fetch(URI.API + "/albums/" + artist.id)
            .then(response => {
                response.json().then(function(data) {
                    self.setState({
                        albums: data.items
                    })
                });
            });
    }
    render(){
        let { artist } = this.props ;
        return(
            <div>
                <div className="headerArtist">
                    <img src={artist.images[0].url} />
                    <span className="artistName">{ artist.name }</span>
                </div>
                <hr />
                {
                    this.state.albums &&
                        this.state.albums.map(function(o, i){
                            return (
                                <Album key={o.id} album={o}/>
                            )
                        })
                }
            </div>
        )
    }
}

let Album = ({album}) => {
    return (
        <div className="album">
            <img src={album.images[2].url} />
            <span className="albumName">{ album.name } </span>
        </div>
    )
}

const mapStateToProps = (state, ownProps) => {
    const backupArtist = {
        name: "Noname",
        id: ownProps.match.params.id,
        images: [{url: "https://dsmpublicartfoundation.org/wp-content/uploads/2016/04/artist_photo_placeholder.png"}]
    }
    return {
        artist: state.artist.hasOwnProperty('artist') ? state.artist.artist : backupArtist
    }

}

Artist = connect(
    mapStateToProps,
)(Artist);

export default Artist ;