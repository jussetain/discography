import React from 'react'
import "./SearchModule.css"
import UpperBox from "./UpperBox"
import Item from "./Item"
import { connect } from 'react-redux'
import { pushArtist } from '../../actions'
import URI from "../../constants/URI"

/*
    Component for the search module (search bar and results)
 */
class SearchModule extends React.PureComponent {
    constructor(props){
        super(props);
        this.state = {
            results: []
        }
        this.onItemClicked = this.onItemClicked.bind(this);
    }

    onInputChange = (event) => {
        let input = event.target.value;
        let self = this;
        if (input.length > 0){
            fetch(URI.API + "/artists/" + input)
                .then(response => {
                    response.json().then(function(data) {
                        console.log(data);
                        if(!data.statusCode){
                            self.setState({
                                results: data.artists && data.artists.items
                            });
                        }
                    });
                });
        } else {
            this.setState({
                results: []
            })
        }
    }

    onItemClicked = (artist) => {
        let { dispatch } = this.props ;
        dispatch(pushArtist(artist));
    }

    getImageOrPlaceholder(o){
        return o.images.length > 0
            ? o.images[0].url
            : "https://dsmpublicartfoundation.org/wp-content/uploads/2016/04/artist_photo_placeholder.png";
    }

    render(){
        let self = this;
        return (
            <div>
                <UpperBox onInputChange={this.onInputChange} />
                <div  className="results">
                    { this.state.results.map(function(o, i){
                        var imageUrl = self.getImageOrPlaceholder(o);
                        return (
                            <Item
                                key={o.id}
                                image={imageUrl}
                                artist={o}
                                onClick={self.onItemClicked}
                            />
                        )
                    }) }
                </div>
            </div>
        )
    }
}

export default connect()(SearchModule)
