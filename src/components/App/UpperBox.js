import React from 'react'
import logo from "../../assets/logospotify.png"

/*
    Component for the search bar and logo
 */
let UpperBox = ({ onInputChange }) => {
    return (
        <div className="box">
            <div className="header">
                <img src={logo}/>
            </div>
            <div className="bar">
                <input autoFocus={true} className="searchBar" type={"text"} placeholder={"Search an artist..."} onChange={onInputChange}/>
            </div>
        </div>
    )
};

export default UpperBox ;