import React from 'react'
import "./App.css"
import SearchModule from "./SearchModule";

/*
    Global component
 */
export default class App extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div className="home">
                <SearchModule />
            </div>
        )
    }
}