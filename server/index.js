var express = require('express');
var app = express();
var Spotify = require('node-spotify-api');
var server = require('http').createServer(app);
var accents = require('remove-accents');


var spotify = new Spotify({
    id: '2aa1fea667354553b52dd3dc2d376fad',
    secret: '233f0b2cc7a440d3bd4cb2073fcacc9b',
});

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
    next();
});


app.get('/artists/:q', function(req, res) {
    console.log("/artists/" + req.params.q);
    spotify.search({ type: 'artist', query: accents.remove(req.params.q) }, function(err, data) {
        if (err) {
            console.log(err);
            res.status(500).send(err);
        } else {
            res.status(200).send(data);
        }
    });
});

app.get('/albums/:q', function(req, res) {
    console.log("/albums/" + req.params.q);
    spotify.request('https://api.spotify.com/v1/artists/' + req.params.q + '/albums/', function(err, data) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).send(data);
        }

    });

});

server.listen(3333, function(){
    console.log("Running...")
});