import reducer from '../../reducers/artist'
import { Reducer } from 'redux-testkit'
import ActionTypes from '../../constants/ActionsTypes'


describe('artist reducer', () => {
    it('should store artist', () => {
        const artist = {id: '123', name: 'michel drucker'};
        const action = {type: ActionTypes.PUSH_ARTIST, payload: artist};
        Reducer(reducer).expect(action).toReturnState({artist});
    });
});