'use strict';
import React from 'react';
import UpperBox from '../../../components/App/UpperBox';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
    const tree = renderer
        .create(<UpperBox />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});